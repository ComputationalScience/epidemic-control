# -*- coding: utf-8 -*-
"""
Created on Tue May 11 09:58:09 2021

@author: 15000
"""

import torch
import random
from network_model import TwoLayerNet
import pdb
import pandas as pd
import copy
import csv
from time import time

data1 = csv.reader(open('stochastic_block_pl.csv'))
index = []
j = 0
for i in data1:
    if float(i[0]) != 0:
        index.append((float(i[0]), j))
    
    j += 1

index.sort()

#S_original = [(i+1)**(-2.9) for i in range(8)]
#summSori = sum(S_original)
#index = [(S_original[i]/summSori, i+1) for i in range(8)]
#S0 = [(1-1e-6)*S_original[i] / summSori for i in range(8)]
#IU0 = [S_original[i] * 1e-6 for i in range(8)]
#IT0 = [0 for i in range(8)]

Action_space = []
def generator(b, ind, remains, f_max, ender):
    global index
    if ind == ender:
        if b:
            Action_space.append((b, ind, remains))
    else:
        b_cop = copy.deepcopy(b)
        if remains < f_max * index[ind][0]:
            Action_space.append((b_cop, ind, remains))
        else:
            b_copp = copy.deepcopy(b)
            b_copp.append(ind)
            generator(b_copp, ind+1, remains - f_max * index[ind][0], f_max, ender)
            generator(b_cop, ind+1, remains, f_max, ender)
    
    return

F0 = 0.003
f_max = 0.9998
f_min = 0.0002
remains = F0 - f_min
generator([], 0, remains, f_max, len(index)-1)
Actions = []
ll = len(index)
#print(index)
#print(Action_space)

#pdb.set_trace()
for i in Action_space:
    j = []
    for ell in i[0]:
        j.append((ell, f_max))
    
    if i[1] == len(index) - 1:
        for rr in range(len(index)):
            if index[i[1]-rr] not in i[0]:
                indd = i[1]-rr
                if i[2] < min(index[i[1]-rr][0], f_max):
                    
                    result = copy.deepcopy(j)
                    result.append((indd, i[2]))
                    Actions.append(result)
    else:
        for rr in range(i[1]+1):
            if index[i[1]-rr] not in i[0]:
                indd = i[1]-rr
                if i[2] < min(index[i[1]-rr][0], f_max):
                    result = copy.deepcopy(j)
                    result.append((indd, i[2]))
                    Actions.append(result)
        for rr in range(i[1]+1, ll):
            if index[rr] not in i[0]:
                #indd = ll-rr-1
                if i[2] < min(index[rr][0], f_max):
                    result = copy.deepcopy(j)
                    result.append((rr, i[2]))
                    Actions.append(result)

#print(index)
#print(len(Actions))
#pdb.set_trace()


length = len(Actions)
#print(Actions)




def action_vaccine(Action_space, index):
    for i in Action_space:
        j = []
        for ell in i[0]:
            j.append((ell, f_max))
    
        if i[1] == len(index) - 1:
            for rr in range(len(index)):
                if index[i[1]-rr] not in i[0]:
                    indd = i[1]-rr
                    if i[2] < min(index[i[1]-rr][0], f_max):
                    
                        result = copy.deepcopy(j)
                        result.append((indd, i[2]))
                        Actions.append(result)
                    else:
                        for rr in range(i[1]+1):
                            if index[i[1]-rr] not in i[0]:
                                indd = i[1]-rr
                                if i[2] < min(index[i[1]-rr][0], f_max):
                                    result = copy.deepcopy(j)
                                    result.append((indd, i[2]))
                                    Actions.append(result)
                        for rr in range(i[1]+1, ll):
                            if index[rr] not in i[0]:
                                #indd = ll-rr-1
                                if i[2] < min(index[rr][0], f_max):
                                    result = copy.deepcopy(j)
                                    result.append((rr, i[2]))
                                    Actions.append(result)

#F0 = 0.005
#f_max = 0.995
#f_min = 0.001
#remains = F0 - f_min
#generator([], 0, remains, f_max, len(index)-1)
#Actions = []
#ll = len(index)
#print(index)
#print(Action_space)

#pdb.set_trace()
def strategy_generator(Action_space, index):
    global f_max, remains
    Actions = []
    for i in Action_space:
        j = []
        for ell in i[0]:
            j.append((ell, float(min(f_max, index[ell]))))
            
        if i[1] == len(index) - 1:
            for rr in range(len(index)):
                if index[i[1]-rr] not in i[0]:
                    indd = i[1]-rr
                    if i[2] < min(f_max, index[i[1] - rr]):
                            
                        result = copy.deepcopy(j)
                        result.append((indd, i[2]))
                        Actions.append(result)
        else:
            for rr in range(i[1]+1):
                if index[i[1]-rr] not in i[0]:
                    indd = i[1]-rr
                    if i[2] < min(f_max, index[i[1] - rr]):
                            
                        result = copy.deepcopy(j)
                        result.append((indd, i[2]))
                        Actions.append(result)
            for rr in range(i[1]+1, ll):
                if index[rr] not in i[0]:
                    #indd = ll-rr-1
                    if i[2] < min(f_max, index[rr]):
                        result = copy.deepcopy(j)
                        result.append((rr, i[2]))
                        Actions.append(result)
    
    total = 0
    for i in range(len(index)):
        total += min(index[i], f_max)
    
    if total <  remains:
        Actions.append((i, min(index[i], f_max)) for i in range(len(index)))
    
    #print(Actions)
    #pdb.set_trace()
    return Actions
        

#print(index)
#print(len(Actions))
#pdb.set_trace()


length = len(Actions)
#pdb.set_trace()

        
correlation = [[0 for i in range(len(index))] for j in range(len(index))]
data2 = csv.reader(open('stochastic_block_plk.csv'))
storage = []
for i in data2:
    storage.append(i)
    
for i in range(len(index)):
    for j in range(len(index)):
        correlation[i][j] = float(storage[index[i][1]][index[j][1]])
        
P = correlation
#rho = [0 for i in range(8)]
#for i in range(8):
#    rho[i] = S0[i] * (i+1)

#sum_rho = sum(rho)
#P =[[0 for j in range(8)] for i in range(8)]
#for i in range(8):
#    for j in range(8):
#        P[i][j] = (j+1) * S0[j] / sum_rho
K = len(index)
#print(index)
#pdb.set_trace()
# Construct the action space consisting of finite strategies, f_m = kf_0, k integer
# set up neural network for evaluating Q function Q(s, a, \theta)
N, D_in, H, D_out = length, 3*K, 30, 1
model = TwoLayerNet(D_in, H, D_out)
model2 = TwoLayerNet(D_in, H, D_out)
Action_space = Actions
# Social network structure P(k|m)
#rho = [0 for i in range(8)]
#for i in range(8):
#    rho[i] = 1



# RK2 for numerically solving the dynamics of disease spreading
def progress(s, iu, it, P, K, a, dt):
    global muu, mut, beta_u, beta_t, f_min
    F_test = []
    for i in range(K):
        if sum(s) > 0:
            F_test.append(f_min * float(s[i])/float(sum(s)))
        else:
            F_test.append(0)
    
    
    #print(sum(s))
    #pdb.set_trace()
    if a[0]:
        #print(a)
        for i in range(len(a)):
            F_test[a[i][0]] += a[i][1]
    
    #print(sum(F_test), a)
    #pdb.set_trace()
    new_infect = [0 for i in range(K)]
    new_infect1 = [0 for i in range(K)]
    diu = [0 for i in range(K)]
    dit = [0 for i in range(K)]
    
    for i in range(K):
        for j in range(K):
            new_infect[j] += s[j] * (index[j][1]) * dt * P[j][i] * (beta_u * iu[i] + beta_t * it[i]) / S_original[i]
    
    for j in range(K):
        s[j] -= new_infect[j] 
    
    for i in range(K):
        diu[i] = new_infect[i] - muu * iu[i] *dt 
        dit[i] = 0
        iu[i] += diu[i]
        it[i] += dit[i]
        
    
    for i in range(K):
        for j in range(K):
            new_infect1[j] += s[j] * (index[j][1]) * dt * P[j][i] * (beta_u * iu[i] + beta_t * it[i]) / S_original[i]
    
    for j in range(K):
        
        s[j] -= min((new_infect1[j]/2 - new_infect[j]/2) + F_test[j], s[j])
    
    for i in range(K):
        iu[i] += new_infect1[i]/2 - dt * muu * iu[i]/2  - diu[i]/2
        it[i] += 0

    return s, iu, it, sum(new_infect)


summ = 0
summ1 = 0
summ2 = 0

for i in range(len(index)):
    summ += (index[i][1]) * index[i][0]
    summ1 += index[i][0]
    summ2 += (index[i][1])**2 * index[i][0]
# Initial setup
summ2 -= summ**2
#print(K)
#pdb.set_trace()
S_original = [index[i][0] / summ1 for i in range(K)]

T = 100
dt = 1
epsilon = 0.2
gamma = 0.95
batch_size = 2
f0 = 1
s = torch.tensor([(1-10**(-6))*S_original[i] for i in range(K)])
iu = torch.tensor([(10**(-6))*S_original[i] for i in range(K)])
it = torch.tensor([0. for i in range(K)])
muu = 0.091
mut = muu
R0 = 2.91
beta_u = muu / ((summ - 1 + summ2/summ) / R0 - 1)
beta_t = beta_u / 10
epoch = 50
replay_buffer = []
Indeces = [i for i in range(T)]
criterion = torch.nn.MSELoss(reduction='sum')
optimizer = torch.optim.SGD(model.parameters(), lr=0.001)
#dt = 1

# Proceed in one epoch to record rewards and Q values, e-greedy for choosing strategies
def forward_step(s, iu, it, t, key=0):
    global P, model, dt, F0
    global progress
    def generator_vaccine(b, ind, remains, f_max, ender, S):
        nonlocal Action_space
        if ind == ender:
            if b:
                Action_space.append((b, ind, remains))
        else:
        
            b_cop = copy.deepcopy(b)
            if remains < min(f_max, S[ind]):
            
                Action_space.append((b_cop, ind, remains))
            #pdb.set_trace()
            else:
                b_copp = copy.deepcopy(b)
                b_copp.append(ind)
                if float(min(f_max, S[ind])) > 0:
                    generator_vaccine(b_copp, ind+1, remains - float(min(f_max, S[ind])), f_max, ender, S)
                
                generator_vaccine(b_cop, ind+1, remains, f_max, ender, S)
            
        return
    
    remains0 = F0 - f_min * sum(s)
    s_ooo = copy.deepcopy(s)
    summm = sum(s_ooo)
    for i in range(len(s_ooo)):
        s_ooo[i]-= s_ooo[i] / summm * f_min
        
    Action_space = []
    generator_vaccine([], 0, remains0, f_max, len(index) - 1, copy.deepcopy(s_ooo))
    Action = strategy_generator(Action_space, copy.deepcopy(s))
    Action_space = Action
    #print(len(Action))
    #pdb.set_trace()
    X = torch.zeros(len(Action_space), 3*K)
    length = len(Action_space)
    if length == 1:
        Action_space.append(copy.deepcopy(Action_space[0]))
    length = len(Action_space)
    X = torch.zeros(len(Action_space), 3*K)
    for i in range(length):
        s_copy = torch.tensor([float(s[l]) for l in range(len(s))])
        iu_copy = torch.tensor([float(iu[l]) for l in range(len(s))])
        it_copy = torch.tensor([float(it[l]) for l in range(len(s))])
        s_new, iu_new, it_new, total_infect = progress(s_copy, iu_copy, it_copy, P, K, Action_space[i], dt)
        for j in range(K):
            X[i, j] = s_new[j]
            X[i, j+K] = iu_new[j]
            X[i, j+2*K] = it_new[j]
    
    #print(Action_space)
    y_eval = model(X)
    ep = random.random()
    if ep > epsilon:
        max_Q = max(y_eval)
        for j in range(length):
            if y_eval[j] == max_Q:
                choice = j
    
    else:
        choice = random.randint(0, length-1)
    
    s_copy = torch.tensor([float(s[l]) for l in range(len(s))])
    iu_copy = torch.tensor([float(iu[l]) for l in range(len(s))])
    it_copy = torch.tensor([float(it[l]) for l in range(len(s))])
    #print(choice)
    #print(len(Action_space))
    s_new, iu_new, it_new, total_infect = progress(s_copy, iu_copy, it_copy, P, K, Action_space[choice], dt)
    buffer = ((s, iu, it), (s_new, iu_new, it_new), choice, -total_infect, t)
    if key == 1:
        pass
        
    return buffer

# Proceed in one epoch based on specified action a
def forward_step_action0(s, iu, it, F0, t):
    global P, length, model, dt, f_max
    global progress
    
    

    #s_copy = torch.tensor([float(s[l]) for l in range(len(s))])
    #iu_copy = torch.tensor([float(iu[l]) for l in range(len(s))])
    #it_copy = torch.tensor([float(it[l]) for l in range(len(s))])
    #print(remains)
    #pdb.set_trace()
    #if sum(s_copy) > 0:
    #    a = [(i, s_copy[i] *  remains / sum(s_copy)) for i in range(K)]
    #else:
    #    a = [(i, 0) for i in range(K)]
        
    s_new, iu_new, it_new, total_infect = progress(s_copy, iu_copy, it_copy, P, K, F0, dt)
    buffer = ((s, iu, it), (s_new, iu_new, it_new), a, -total_infect, t)
    return buffer

def forward_step_action(s, iu, it, F0, t):
    global P, length, model, dt, f_max
    global progress
    def generator_vaccine(b, ind, remains, f_max, ender, S):
        nonlocal Action_space
        if ind == ender:
            if b:
                Action_space.append((b, ind, remains))
        else:
        
            b_cop = copy.deepcopy(b)
            if remains < min(f_max, S[ind]):
            
                Action_space.append((b_cop, ind, remains))
            #pdb.set_trace()
            else:
                b_copp = copy.deepcopy(b)
                b_copp.append(ind)
                if float(min(f_max, S[ind])) > 0:
                    generator_vaccine(b_copp, ind+1, remains - float(min(f_max, S[ind])), f_max, ender, S)
                generator_vaccine(b_cop, ind+1, remains, f_max, ender, S)
            
        return
    
    Action_space = []
    remains0 = f_min * sum(s)
    s_ooo = copy.deepcopy(s)
    summm = sum(s_ooo)
    for i in range(len(s_ooo)):
        s_ooo[i] -= s_ooo[i] / summm * f_min
        
    generator_vaccine([], 0, remains0, f_max, len(index) - 1, copy.deepcopy(s_ooo))
    #print(len(Action_space))
    #pdb.set_trace()
    
    Action = strategy_generator(Action_space, copy.deepcopy(s))
    Action_space = Action
    #print(Action)
    
    #print(S)
    #pdb.set_trace()
    X = torch.zeros(len(Action_space), 3*K)
    length = len(Action_space)
    X = torch.zeros(length, 3*K)
    #print(X)
    #pdb.set_trace()
    s_copy = torch.tensor([float(s[l]) for l in range(len(s))])
    iu_copy = torch.tensor([float(iu[l]) for l in range(len(s))])
    it_copy = torch.tensor([float(it[l]) for l in range(len(s))])
    #print(remains)
    #pdb.set_trace()
    if sum(s_copy) > 0:
        a = [(i, s_copy[i] *  remains / sum(s_copy)) for i in range(K)]
    else:
        a = [(i, 0) for i in range(K)]
        
    s_new, iu_new, it_new, total_infect = progress(s_copy, iu_copy, it_copy, P, K, a, dt)
    #for j in range(K):
    #    X[i, j] = s_new[j]
    #    X[i, j+K] = iu_new[j]
    #    X[i, j+2*K] = it_new[j]
    
    buffer = ((s, iu, it), (s_new, iu_new, it_new), a, -total_infect, t)
    return buffer

# Proceed in one epoch to record rewards and Q values, without e-greedy for choosing strategies
def forward_step_deterministic(s, iu, it, t, key=0):
    global P, model, dt, F0
    global progress
    def generator_vaccine(b, ind, remains, f_max, ender, S):
        nonlocal Action_space
        if ind == ender:
            if b:
                Action_space.append((b, ind, remains))
        else:
        
            b_cop = copy.deepcopy(b)
            if remains < min(f_max, S[ind]):
            
                Action_space.append((b_cop, ind, remains))
            #pdb.set_trace()
            else:
                b_copp = copy.deepcopy(b)
                b_copp.append(ind)
                if float(min(f_max, S[ind])) > 0:
                    generator_vaccine(b_copp, ind+1, remains - float(min(f_max, S[ind])), f_max, ender, S)
                
                generator_vaccine(b_cop, ind+1, remains, f_max, ender, S)
            
        return
    
    remains0 = F0 - f_min * sum(s)
    s_ooo = copy.deepcopy(s)
    summm = sum(s_ooo)
    for i in range(len(s_ooo)):
        s_ooo[i]-= s_ooo[i] / summm * f_min
        
    Action_space = []
    generator_vaccine([], 0, remains0, f_max, len(index) - 1, copy.deepcopy(s_ooo))
    Action = strategy_generator(Action_space, copy.deepcopy(s))
    Action_space = Action
    #print(len(Action))
    #pdb.set_trace()
    X = torch.zeros(len(Action_space), 3*K)
    length = len(Action_space)
    if length == 1:
        Action_space.append(copy.deepcopy(Action_space[0]))
    length = len(Action_space)
    X = torch.zeros(len(Action_space), 3*K)
    for i in range(length):
        s_copy = torch.tensor([float(s[l]) for l in range(len(s))])
        iu_copy = torch.tensor([float(iu[l]) for l in range(len(s))])
        it_copy = torch.tensor([float(it[l]) for l in range(len(s))])
        s_new, iu_new, it_new, total_infect = progress(s_copy, iu_copy, it_copy, P, K, Action_space[i], dt)
        for j in range(K):
            X[i, j] = s_new[j]
            X[i, j+K] = iu_new[j]
            X[i, j+2*K] = it_new[j]
    
    #print(Action_space)
    y_eval = model(X)
    ep = random.random()
    max_Q = max(y_eval)
    for j in range(length):
        if y_eval[j] == max_Q:
            choice = j
    
    s_copy = torch.tensor([float(s[l]) for l in range(len(s))])
    iu_copy = torch.tensor([float(iu[l]) for l in range(len(s))])
    it_copy = torch.tensor([float(it[l]) for l in range(len(s))])
    #print(choice)
    #print(len(Action_space))
    s_new, iu_new, it_new, total_infect = progress(s_copy, iu_copy, it_copy, P, K, Action_space[choice], dt)
    buffer = ((s, iu, it), (s_new, iu_new, it_new), choice, -total_infect, t)
    if key == 1:
        pass
        
    return buffer

s = torch.tensor([(1-10**(-6))*S_original[i] for i in range(K)])
iu = torch.tensor([(10**(-6))*S_original[i] for i in range(K)])
it = torch.tensor([0. for i in range(K)])
s_copy = [float(s[i]) for i in range(K)]
iu_copy = [float(iu[i]) for i in range(K)]
it_copy = [float(it[i]) for i in range(K)]
strategy = []
S = []
IU = []
IT = []
S.append(s_copy)
IU.append(iu_copy)
IT.append(it_copy)
a = [(i, remains) for i in range(28)]
re = 0
total = [0]
# Proceed with the optimal strategy
for j in range(T):
    print(j)
    buffer = forward_step_action(s, iu, it, remains, j)
    #pdb.set_trace()
    s = buffer[1][0]
    iu = buffer[1][1]
    it = buffer[1][2]
    re += buffer[3]
    total.append(float(re))
    s_copy = [0 for i in range(K)]
    iu_copy = [0 for i in range(K)]
    it_copy = [0 for i in range(K)]
    for i in range(K):
        s_copy[i] = float(s[i])
        iu_copy[i] = float(iu[i])
        it_copy[i] = float(it[i])
        
    S.append(s_copy)
    IU.append(iu_copy)
    IT.append(it_copy)
    #print(sum(s_copy))
    strategy.append(buffer[2])

#print(sum(s_copy))
testing_s = pd.DataFrame(data = S)
testing_s.to_csv('RL_s_sum_vaccine_compare3_sb.csv', header=False, index = False)
testing_iu = pd.DataFrame(data = IU)
testing_iu.to_csv('RL_iu_sum_vaccine_compare3_sb.csv', header=False, index = False)
testing_it = pd.DataFrame(data = IT)
testing_it.to_csv('RL_it_sum_vaccine_compare3_sb.csv', header=False, index = False)
testing_strategy = pd.DataFrame(data = strategy)
testing_strategy.to_csv('RL_strategy_sum_vaccine_compare3_sb.csv', header=False, index = False)
testing_total = pd.DataFrame(data = total)
testing_total.to_csv('RL_total_sum_vaccine_compare3_sb.csv', header=False, index = False)
#pdb.set_trace()

#s = torch.tensor([(1-10**(-6))*S_original[i]/summSori for i in range(K)])
#iu = torch.tensor([(10**(-6))*S_original[i]/summSori for i in range(K)])
s = torch.tensor([(1-10**(-6))*S_original[i] for i in range(K)])
iu = torch.tensor([(10**(-6))*S_original[i] for i in range(K)])
it = torch.tensor([0. for i in range(K)])
s_copy = [float(s[i]) for i in range(K)]
iu_copy = [float(iu[i]) for i in range(K)]
it_copy = [float(it[i]) for i in range(K)]
# training, experience replay, two networks Q(,theta) and Q(,theta^-)
for i in range(epoch):
    if i == 0:
        for j in range(T):
            print(j)
            buffer = forward_step(s, iu, it, j)
            s = buffer[1][0]
            iu = buffer[1][1]
            it = buffer[1][2]
            replay_buffer.append(buffer)
    else:
        t = time()
        s = torch.tensor([(1-10**(-6))*S_original[i] for i in range(K)])
        iu = torch.tensor([(10**(-6))*S_original[i] for i in range(K)])
        it = torch.tensor([0. for i in range(K)])
        for j in range(T):
            if j % 2 == 0:
                torch.save(model.state_dict(),'net_params_vaccine_sb.pkl')
                model2.load_state_dict(torch.load('net_params_vaccine_sb.pkl'))
                
            buffer = forward_step(s, iu, it, j)
            s = buffer[1][0]
            iu = buffer[1][1]
            it = buffer[1][2]
            random.shuffle(Indeces)
            x = []
            for l in range(batch_size):
                x.append(Indeces[l])
            
            train_data = []
            target = []
            rewards = []
            rewards_next = []
            for r in range(batch_size):
                train1 = [0 for i in range(3*K)]
                train2 = [0 for i in range(3*K)]
                buffer_next = forward_step_action0(replay_buffer[x[r]][1][0], replay_buffer[x[r]][1][1], replay_buffer[x[r]][1][2], replay_buffer[x[r]][1][2], replay_buffer[x[r]][4]+1)
                rewards_next.append(buffer_next[3])
                for i in range(K):
                    train1[i] = float(replay_buffer[x[r]][1][0][i])
                    train1[i+K] = float(replay_buffer[x[r]][1][1][i])
                    train1[i+2*K] = float(replay_buffer[x[r]][1][2][i])
                    
                    train2[i] = float(buffer_next[1][0][i])
                    train2[i+K] = float(buffer_next[1][1][i])
                    train2[i+2*K] = float(buffer_next[1][1][i])
                train_data.append(train1)
                target.append(train2)
                rewards.append(replay_buffer[x[r]][3])
            
            rewards = torch.tensor(rewards)
            rewards_next = torch.tensor(rewards_next)
            #print(target)
            y_compare = (model2(torch.tensor(target)) *gamma + rewards_next) * gamma + rewards
            for r in range(batch_size):
                if replay_buffer[r][4] == T-1:
                    y_compare[r] = rewards[r]
            
            
            y_aim = model(torch.tensor(train_data)) * gamma + rewards
            #print(y_compare)
            #print(y_aim)
            #pdb.set_trace()
            loss = criterion(y_aim, y_compare)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            replay_buffer.append(buffer)
            replay_buffer.pop(0)
        
        print(loss.item(), time() - t)

s = torch.tensor([(1-10**(-6))*S_original[i] for i in range(K)])
iu = torch.tensor([(10**(-6))*S_original[i] for i in range(K)])
it = torch.tensor([0. for i in range(K)])
s_copy = [float(s[i]) for i in range(K)]
iu_copy = [float(iu[i]) for i in range(K)]
it_copy = [float(it[i]) for i in range(K)]
strategy = []
S = []
IU = []
IT = []
S.append(s_copy)
IU.append(iu_copy)
IT.append(it_copy)
re = 0
total = [0]
# Proceed with the optimal strategy
for j in range(T):
    buffer = forward_step_deterministic(s, iu, it, j)
    s = buffer[1][0]
    iu = buffer[1][1]
    it = buffer[1][2]
    re += buffer[3]
    total.append(float(re))
    s_copy = [0 for i in range(K)]
    iu_copy = [0 for i in range(K)]
    it_copy = [0 for i in range(K)]
    for i in range(K):
        s_copy[i] = float(s[i])
        iu_copy[i] = float(iu[i])
        it_copy[i] = float(it[i])
        
    S.append(s_copy)
    IU.append(iu_copy)
    IT.append(it_copy)
    strategy.append(buffer[2])

testing_s = pd.DataFrame(data = S)
testing_s.to_csv('RL_s_sum3_sb.csv', header=False, index = False)
testing_iu = pd.DataFrame(data = IU)
testing_iu.to_csv('RL_iu_sum3_sb.csv', header=False, index = False)
testing_it = pd.DataFrame(data = IT)
testing_it.to_csv('RL_it_sum3_sb.csv', header=False, index = False)
testing_strategy = pd.DataFrame(data = strategy)
testing_strategy.to_csv('RL_strategy_sum3_sb.csv', header=False, index = False)
testing_total = pd.DataFrame(data = total)
testing_total.to_csv('RL_strategy_total3_sb.csv', header=False, index = False)
#s = torch.tensor([(1-10**(-5))*1/8 for i in range(8)])
#iu = torch.tensor([(10**(-5))*1/8 for i in range(8)])
#it = torch.tensor([0. for i in range(8)])