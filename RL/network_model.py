# -*- coding: utf-8 -*-
"""
Created on Tue Dec 22 03:01:57 2020

@author: 15000
"""
import torch
from math import sqrt


class TwoLayerNet(torch.nn.Module):
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, H)
        self.renorm4 = torch.nn.BatchNorm1d(H)
        self.linear5 = torch.nn.Linear(H, D_out)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = self.renorm1(h_relu1)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = self.renorm2(h_relu2)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = self.renorm3(h_relu3)
        h_relu3 = torch.relu(h_relu3)
        h_relu4 = self.linear4(h_relu3)
        h_relu4 = self.renorm4(h_relu4)
        y_pred = self.linear5(h_relu4)
        return y_pred