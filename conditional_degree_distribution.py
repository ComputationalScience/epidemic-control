import numpy as np
import networkx as nx
import collections

def conditional_degree_distribution(G, uncorrelated = True):
    """ 
    Calculates the conditional degree distribution P(l|k) of a graph G.
    
    Parameters: 
    G (graph): networkx graph object 
    uncorrelated (bool): True (uncorrelated approximation), 
    False (no approximation)
    Returns: 
    function: conditional degree distribution P(l|k), 
    degree distribution P(k), maximum degree
  
    """
    
    if uncorrelated == True:
        
        degree_sequence = sorted([d for n, d in G.degree()], reverse=True)
    
        degreeCount = collections.Counter(degree_sequence)
        deg, cnt = zip(*degreeCount.items())
        cnt = np.asarray(cnt)/sum(cnt)
        deg = np.asarray(deg)
        
        cnt2 = []
        deg2 = []
        for i in range(max(deg)+1):
            try:
                cnt2.append(cnt[deg == i][0])
            except:
                cnt2.append(0)
            deg2.append(i)
            
        cnt2 = np.asarray(cnt2)
        deg2 = np.asarray(deg2)
        
        p_l = lambda l: cnt2[deg2 == l][0]
        
        mean_degree = sum(p_l(l)*l for l in deg2)

        p_lk_conditional = lambda l: l*p_l(l)/mean_degree
                
        return p_lk_conditional, p_l, max(deg2)
    
    else: 
        
        plk_matrix = nx.degree_mixing_matrix(G, normalized = True)
    
        plk_joint_distribution = lambda l,k: plk_matrix[l,k]
        
        degree_sequence = sorted([d for n, d in G.degree()], reverse=True)
    
        degreeCount = collections.Counter(degree_sequence)
        deg, cnt = zip(*degreeCount.items())
        cnt = np.asarray(cnt)/sum(cnt)
        deg = np.asarray(deg)
        
        cnt2 = []
        deg2 = []
        for i in range(max(deg)+1):
            try:
                cnt2.append(cnt[deg == i][0])
            except:
                cnt2.append(0)
            deg2.append(i)
            
        cnt2 = np.asarray(cnt2)
        deg2 = np.asarray(deg2)
        
        p_l = lambda l: cnt2[deg2 == l][0]
                
        mean_degree = sum(p_l(l)*l for l in deg2)
        
        # check if sum over joint distribution gives distribution over edge ends 
        # according to appendix A
        # print(sum(plk_matrix[:,100]), p_l(100)*100/mean_degree)

        p_lk_conditional = lambda l,k: mean_degree*\
                           plk_joint_distribution(l,k)/(p_l(k)*k)
        
        return p_lk_conditional, p_l, max(deg2)

if __name__ == "__main__":
    
    # generate ER network
    N = 1000
    p = 0.1
    G = nx.erdos_renyi_graph(N, p, seed=1)
    p_lk_conditional, p_l, max_degree = conditional_degree_distribution(G, uncorrelated=True)
