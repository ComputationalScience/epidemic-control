import copy
import pdb
import pandas as pd
import csv

data1 = csv.reader(open('barabasi_albert_pl_full.csv'))
index = []
j = 0
ind_sum = 0
for i in data1:
    if float(i[0]) != 0:
        index.append((float(i[0]), j))
    
    if j != 0:
        ind_sum += float(i[0])
    j += 1


#for i in range(len(index)):
#    if i != 0:
#        index[i] = (float(index[i][0] / ind_sum), i)
#    else:
#        index[i] = (0, 0)

#index.pop(0)
#index.sort()
#Action_space = []
#def generator(b, ind, remains, f_max, ender):
#    global index
#    if ind == ender:
#        if b:
#            Action_space.append((b, ind, remains))
#    else:
#        b_cop = copy.deepcopy(b)
#        if remains < f_max * index[ind + 1][0]:
#            Action_space.append((b_cop, ind, remains))
#        else:
#            b_copp = copy.deepcopy(b)
#            b_copp.append(ind+1)
#            generator(b_copp, ind+1, remains - f_max * index[ind + 1][0], f_max, ender)
#            generator(b_cop, ind+1, remains, f_max, ender)
    
#    return

#remains = 0.01
#f_max = 0.2
#generator([], 0, remains, f_max, len(index)-1)
#print(Action_space)
#pdb.set_trace()
#Actions = []
#ll = len(index)
#print(len(Action_space))
#pdb.set_trace()
#for i in Action_space:
#    j = []
#    for ell in i[0]:
#        j.append((ell, f_max))
    
#    if i[1] == len(index) - 1:
#        for rr in range(len(index)):
#            if index[ll-rr-1] not in i[0]:
#                indd = ll-rr-1
#                if i[2] < index[ll-rr-1][0] * f_max:
#                    result = copy.deepcopy(j)
#                    result.append((indd, i[2] / index[ll-rr-1][0]))
#                    Actions.append(result)
#    else:
#        for rr in range(i[1]):
#            if index[i[1]-rr-1] not in i[0]:
#                indd = ll-rr-1
#                if i[2] < index[ll-rr-1][0] * f_max:
#                    result = copy.deepcopy(j)
#                    result.append((indd, i[2] / index[ll-rr-1][0]))
#                    Actions.append(result)
#        for rr in range(len(index), ll):
#            if index[rr] not in i[0]:
                #indd = ll-rr-1
#                if i[2] < index[rr] * f_max:
#                    result = copy.deepcopy(j)
#                    result.append((rr, i[2] / index[rr][0]))
#                    Actions.append(result)


        
#print(len(Actions))
#pdb.set_trace()

        
correlation = [[0 for i in range(len(index))] for j in range(len(index))]
data1 = csv.reader(open('barabasi_albert_plk_full.csv'))
storage = []
for i in data1:
    storage.append(i)
    
for i in range(len(index)):
    for j in range(len(index)):
        correlation[i][j] = float(storage[index[i][1]][index[j][1]])

summ = 0
summ1 = 0
var = 0
for i in range(len(index)):
    summ += (index[i][1]) * index[i][0]
    var += (index[i][1])**2 * index[i][0]
    summ1 += index[i][0]
    
var -= summ**2
#print(summ, var, index)
#pdb.set_trace()
def ODEsolver(n, S, IU, IT, F, t, dt):
    global S_original
    new_infect = [0 for i in range(n)]
    new_infect1 = [0 for i in range(n)]
    DIU = [0 for i in range(n)]
    DIT = [0 for i in range(n)]
    DIU_new = [0 for i in range(n)]
    DIT_new = [0 for i in range(n)]
    global P, beta_u, beta_t, gamma_u, gamma_t
    m = len(S)
    for i in range(n):
        for j in range(m):
            new_infect[i] += dt* ((index[i][1]) * S[i] * P[j][i] * (beta_u * IU[j]/ S_original[j] + beta_t * IT[j]/ S_original[j])) 
    
    #print(new_infect, sum(new_infect))
    #pdb.set_trace()
    #print(new_infect)
    #pdb.set_trace()
    for i in range(n):
        S[i] -= new_infect[i]
    
    for i in range(n):
        DIU[i] = -(gamma_u * dt + F[i]/S_original[i] * dt) * IU[i] + new_infect[i]
        DIT[i] = -(gamma_u * dt ) * IT[i] + F[i]/S_original[i] * dt * IU[i]
        IU[i] = IU[i] + DIU[i]
        IT[i] = IT[i] + DIT[i]
    
    for i in range(n):
        for j in range(m):
            new_infect1[i] += dt * ((index[i][1]) * S[i] * P[j][i] * (beta_u * IU[j]/ S_original[j] + beta_t * IT[j]/ S_original[j]))
    
    for i in range(n):
        S[i] = S[i] - new_infect1[i] / 2 + new_infect[i] / 2
    
    for i in range(n):
        DIU_new[i] = -(gamma_u * dt + F[i]/S_original[i] * dt) * IU[i] + new_infect1[i]
        DIT_new[i] = -(gamma_u * dt ) * IT[i] + F[i]/S_original[i] * dt * IU[i]
        IU[i] = IU[i] + DIU_new[i] /2 - DIU[i] / 2
        IT[i] = IT[i] + DIT_new[i] / 2 - DIT[i] / 2
    
    #print(IT)
    #pdb.set_trace()
    return (S, IU, IT)
        
def Adjointsolver(n, lambdaS, lambdaIU, lambdaIT, S, IU, IT, F, t, dt):
    DLambdaS = [0 for i in range(n)]
    DLambdaS1 = [0 for i in range(n)]
    DLambdaIU = [0 for i in range(n)]
    DLambdaIT = [0 for i in range(n)]
    DLambdaIU_new = [0 for i in range(n)]
    DLambdaIT_new = [0 for i in range(n)]
    global P, beta_u, beta_t, gamma_u, gamma_t, gamma
    m = len(S)
    #print(S)
    #print(t)
    #pdb.set_trace()
    #pdb.set_trace()
   
    for i in range(n):
        for j in range(m):
            DLambdaS[i] += (gamma**(t-dt) - lambdaS[i] +lambdaIU[i]) * (index[i][1]) * P[j][i] * (beta_u * IU[j] + 
                            beta_t * IT[j]) / S_original[j]  
            DLambdaIU[i] += S[j] * P[i][j] * (index[j][1]) * beta_u * (gamma**t-lambdaS[j] + 
                              lambdaIU[j]) / S_original[i]
            DLambdaIT[i] += S[j] * P[i][j] * (index[j][1]) * beta_t * (gamma**t-lambdaS[j] + lambdaIU[j]) / S_original[i] 
        

        DLambdaIU[i] += - gamma_u * lambdaIU[i] - F[i]/S_original[i] * (lambdaIU[i] - lambdaIT[i])
        DLambdaIT[i] += - gamma_t * lambdaIT[i] 

        
    for i in range(n):
        lambdaS[i] = lambdaS[i] + dt * DLambdaS[i]
        lambdaIU[i] = lambdaIU[i] + dt * DLambdaIU[i]
        lambdaIT[i] = lambdaIT[i] + dt * DLambdaIT[i]
    
    
    for i in range(n):
        for j in range(m):
                DLambdaS1[i] += (gamma**(t-dt) - lambdaS[i] +lambdaIU[i]) * (index[i][1]) * P[j][i] * (beta_u * IU[j] + 
                                   beta_t * IT[j]) / S_original[j] 
                DLambdaIU_new[i] += S[j] * P[i][j] * (index[j][1]) * beta_u * (gamma**(t-dt)-lambdaS[j] +
                              lambdaIU[j]) / S_original[i] 
                DLambdaIT_new[i] += S[j] * P[i][j] * (index[j][1]) * beta_t * (gamma**(t-dt) - lambdaS[j] + lambdaIU[j]) / S_original[i] 
        
        DLambdaIU_new[i] += - gamma_u * lambdaIU[i] - F[i]/S_original[i] * (lambdaIU[i] - lambdaIT[i])
        DLambdaIT_new[i] += - gamma_t * lambdaIT[i]
                                                               
    for i in range(n):  
        lambdaS[i] = lambdaS[i] - dt/2 * DLambdaS[i] + dt/2 * DLambdaS1[i]
        lambdaIU[i] = lambdaIU[i] - dt/2 * DLambdaIU[i] + dt/2 * DLambdaIU_new[i]
        lambdaIT[i] = lambdaIT[i] - dt/2 * DLambdaIT[i] + dt/2 * DLambdaIT_new[i]
    
    return (lambdaS, lambdaIU, lambdaIT)

def Strategy(n, F, S, IU, IT, lambdaS, lambdaIU, lambdaIT, key=0):
    global fmin, fmax, P
    Deter = [0 for i in range(n)]
    #if key == 1:
        #print(lambdaIU, lambdaIT, lambdaS)   
    #    pdb.set_trace()
        
    for i in range(n):
        Deter[i] = -lambdaIU[i] * IU[i]/S_original[i] + lambdaIT[i] * IU[i]/S_original[i]
        #Deter[i] = -Deter[i]
    
    ##if key:
        #print(Deter)
        #pdb.set_trace()
    for i in range(n):
        Deter[i] = (Deter[i], i)
        
    Deter.sort()
    #print(lambdaS, lambdaIU, lambdaIT)
    #pdb.set_trace()
    F = F - fmin
    Budget = [fmin * S_original[i] for i in range(n)]
    i = 0
    #print(sum(S_original))
    #pdb.set_trace()
    while F>0 and i < n:
        if F > (fmax - fmin) * S_original[Deter[i][1]]:
            Budget[Deter[i][1]] += (fmax - fmin) * S_original[Deter[i][1]]
            F -= (fmax-fmin) * S_original[Deter[i][1]]
        else:
            Budget[Deter[i][1]] += F
            F -= (fmax - fmin) * S_original[Deter[i][1]]
        
        i += 1
    
    #print(Budget)
    #pdb.set_trace()
    return Budget

def evaluation(n, S, S_new, t):
    global gamma
    y = 0
    for i in range(n):
        y += (S[i] - S_new[i])
    
    y = y * gamma**t
    return y

def forward_process(n, dt, nt, S0, IU0, IT0, F):
    t = 0
    reward = 0
    global S, IU, IT, S_re
    s_ini = sum(S0)
    for i in range(nt):
        S0_copy = copy.deepcopy(S0)
        Result = ODEsolver(n, S0, IU0, IT0, F[i], t, dt)
        reward += evaluation(n, S0_copy, Result[0], t)
        #print(S0_copy)
        #print(Result[0])
        #print(evaluation(n, S0_copy, Result[0], t))
        #print(sum(Result[0]) - sum(S0_copy))
        #pdb.set_trace()
        S0 = Result[0]
        IU0 = Result[1]
        IT0 = Result[2]
        t += dt
        #print(t)
        S.append(copy.deepcopy(S0))
        IU.append(copy.deepcopy(IU0))
        IT.append(copy.deepcopy(IT0))
        #print(IT0)
        S_re.append(float(s_ini - sum(S0)))
        #pdb.set_trace()
        #print(sum(S0))
    
    #print(S)
    #pdb.set_trace()

    return (S0, IU0, IT0, reward)

def backward_process(n, dt, nt, lambdaS, lambdaIU, lambdaIT, S, IU, IT, F):
    t = T
    global F_total
    LS = []
    LIU = []
    LIT = []
    LS.append(copy.deepcopy(lambdaS))
    LIU.append(copy.deepcopy(lambdaIU))
    LIT.append(copy.deepcopy(lambdaIT))
    for i in range(nt):
        #F_copy = Strategy(n, F_total, S[nt-i], IU[nt-i], IT[nt-i], lambdaS, lambdaIU, lambdaIT)
        Result = Adjointsolver(n, lambdaS, lambdaIU, lambdaIT, S[nt-i], IU[nt-i], IT[nt-i], F[nt-1-i], t, dt)
        lambdaS = Result[0]
        lambdaIU = Result[1]
        lambdaIT = Result[2]
        LS.append(copy.deepcopy(lambdaS))
        LIU.append(copy.deepcopy(lambdaIU))
        LIT.append(copy.deepcopy(lambdaIT))
        t -= dt
        #print(t)
        #print(lambdaS, lambdaIU, lambdaIT)
        #pdb.set_trace()
        #F[nt-i] = F_copy
    
    #print(LIU)
    #pdb.set_trace()
    for i in range(nt+1):
        if i != 1:
            F[nt-i] = Strategy(n, F_total, S[nt-i], IU[nt-i], IT[nt-i], LS[i], LIU[i], LIT[i])
        else:
            F[nt-i] = Strategy(n, F_total, S[nt-i], IU[nt-i], IT[nt-i], LS[i], LIU[i], LIT[i], 1)
        #####
    #print(LIU[2], LIT[2], IU[0])
    #print(lambdaIU)
    #pdb.set_trace()


n = len(index)
S_original = [index[i][0] / summ1 for i in range(n)]#[(1-1e-6)/8 for i in range(8)]
#summSori = sum(S_original)
#S0 = [(1-1e-6)*S_original[i] / summSori for i in range(n)]
S0 = [(1-1e-6)*S_original[i] for i in  range(n)]
IU0 = [S_original[i] * 1e-6 for i in range(n)]
IT0 = [0 for i in range(n)]
S0 = [S_original[i] for i in range(n)]
S0[2] = S0[2]  - 1e-6
IU0 = [0 for i in range(n)]
IU0[2] = 1e-6
gamma = 0.95
lambdaS = [0 for i in range(n)]
lambdaIU = [0 for i in range(n)]
lambdaIT = [0 for i in range(n)]
fmin = 0.00
fmax = 0.4
F_total = 0.006
gamma_u = 1/14
gamma_t = 1/14
R0 = 4.5
beta_u =  0.0417
#print(summ, var, beta_u/gamma_u * (summ + var/summ), beta_u/gamma_u * (summ + var/summ - 1))
#beta_u=0.0093*2
#print(beta_u)
#pdb.set_trace()
#beta_u = gamma_u / ((summ) / R0 )
beta_t = beta_u / 10
rho = [0 for i in range(n)]
for i in range(n):
    rho[i] = S0[i] * (i+1)

sum_rho = sum(rho)
P = correlation
#P =[[0 for j in range(n)] for i in range(n)]
#for i in range(n):
#    for j in range(n):
#        P[i][j] = (j+1) * S0[j] / sum_rho
        


#print(Po)
#pdb.set_trace()
T = 202
dt = 0.1
nt = 2002
#F_total = 0
F = [[F_total * S_original[i] for i in range(n)] for j in range(nt+1)]
S = []
IU = []
IT = []
S_re = []
#print(P)
#pdb.set_trace()
S.append(copy.deepcopy(S0))
S_re.append(0)
IU.append(copy.deepcopy(IU0))
IT.append(copy.deepcopy(IT0))
#print(S0)
Iter1 = forward_process(n, dt, nt, S0, IU0, IT0, F)
#print(S[0])
testing_s = pd.DataFrame(data = S)
testing_s.to_csv('pontryagain_s_assortive1_ba20_IC1new.csv', header=False, index = False)
testing_iu = pd.DataFrame(data = IU)
testing_iu.to_csv('pontryagain_iu_assortive1_ba20_IC1new.csv', header=False, index = False)
testing_it = pd.DataFrame(data = IT)
testing_it.to_csv('pontryagain_it_assortive1_ba20_IC1new.csv', header=False, index = False)
testing_stra = pd.DataFrame(data = F)
testing_stra.to_csv('pontryagain_strategy_assortive1_ba20_IC1new.csv', header=False, index = False)
testing_stra = pd.DataFrame(data = S_re)
testing_stra.to_csv('pontryagain_compare_testing_ba20_IC1new.csv', header=False, index = False)
#pdb.set_trace()
reward0 = Iter1[-1]
optimal = F
optimal_cost = [S, IU, IT]
print(reward0)
#pdb.set_trace()
backward_process(n, dt, nt, lambdaS, lambdaIU, lambdaIT, S, IU, IT, F)

#F_best = copy.deepcopy(F)
S_original = [index[i][0] / summ1 for i in range(n)]#[(1-1e-6)/8 for i in range(8)]
#summSori = sum(S_original)
#S0 = [(1-1e-6)*S_original[i] / summSori for i in range(n)]
S0 = [(1-1e-6)*S_original[i] for i in  range(n)]
IU0 = [S_original[i] * 1e-6 for i in range(n)]
S0 = [S_original[i] for i in range(n)]
S0[2] = S0[2]  - 1e-6
IU0 = [0 for i in range(n)]
IU0[2] = 1e-6
#S0 = [(i+1)**(-2.9) for i in range(n)]#[(1-1e-6)/8 for i in range(8)]
#summS0 = sum(S0)
#S0 = [S0[i] / summS0 for i in  range(n)]
#IU0 = [S0[i] * 1e-6 for i in range(n)]
IT0 = [0 for i in range(n)]
lambdaS = [0 for i in range(n)]
lambdaIU = [0 for i in range(n)]
lambdaIT = [0 for i in range(n)]
S = []
IU = []
IT = []
S.append(copy.deepcopy(S0))
IU.append(copy.deepcopy(IU0))
IT.append(copy.deepcopy(IT0))
Iter2 = forward_process(n, dt, nt, S0, IU0, IT0, F)
reward1 = Iter2[-1]
#print(F)
print(reward1)


#if reward1 < best_reward:
#pdb.set_trace()
best_reward = reward1
F_coppy = []
#if reward1 < best_reward:
for j in range(nt):
        #if j == 1000:
            #print(F[j])
        #pdb.set_trace()
    #print(F[j], [F[j][i] / S_original[i] for i in range(len(F[j]))])
    #pdb.set_trace()
    #print(j)
    F_coppy.append([F[j][i] / S_original[i] for i in range(len(F[j]))])
    #print(F_coppy)
    #pdb.set_trace()
        #if j == 1000:
        #    print(F[j])
        #    pdb.set_trace()
    #print(j)
    
    optimal = copy.deepcopy(F_coppy)
    optimal_cost = [copy.deepcopy(S), copy.deepcopy(IU), copy.deepcopy(IT)]
    #print(F[j])
    #pdb.set_trace()
#else:
backward_process(n, dt, nt, lambdaS, lambdaIU, lambdaIT, S, IU, IT, F)
#pdb.set_trace()
#    F_best = copy.deepcopy(F)    
#    best_reward = reward1

#F_copy = copy.deepcopy(F)
#print(reward0)
#print(reward1)
#print(F[0])
#pdb.set_trace()
#print(optimal[0])
#pdb.set_trace()
k = 0
while abs(reward0 - reward1) > 1e-9 and k < 10:
    k += 1
    print(reward1)
    #if reward1 < 0.65:
    #    print(F)
    #print(F[-2])
    #pdb.set_trace()
    #print(F)  
    #pdb.set_trace()
    reward0 = reward1
    S_original = [index[i][0] / summ1 for i in range(n)]#[(1-1e-6)/8 for i in range(8)]
    #summSori = sum(S_original)
    #S0 = [(1-1e-6)*S_original[i] / summSori for i in range(n)]
    S0 = [(1-1e-6)*S_original[i] for i in  range(n)]
    IU0 = [S_original[i] * 1e-6 for i in range(n)]
    #print(S0)
    #pdb.set_trace()
    IU0 = [S0[i] * 1e-6 for i in range(n)]
    IT0 = [0 for i in range(n)]
    S0 = [S_original[i] for i in range(n)]
    S0[2] = S0[2]  - 1e-6
    IU0 = [0 for i in range(n)]
    IU0[2] = 1e-6
    lambdaS = [0 for i in range(n)]
    lambdaIU = [0 for i in range(n)]
    lambdaIT = [0 for i in range(n)]
    S = []
    IU = []
    IT = []
    S.append(copy.deepcopy(S0))
    IU.append(copy.deepcopy(IU0))
    IT.append(copy.deepcopy(IT0))
    Iter2 = forward_process(n, dt, nt, S0, IU0, IT0, F)
    reward1 = Iter2[-1]
    
    
    if reward1 < best_reward:
        F_coppy = []
        for j in range(nt):
            F_coppy.append([F[j][i] / S_original[i] for i in range(len(F[j]))])
        
        #print(F_coppy)
        #pdb.set_trace()
        optimal = copy.deepcopy(F_coppy)
        optimal_cost = [copy.deepcopy(S), copy.deepcopy(IU), copy.deepcopy(IT)]
    #if reward1 > best_reward:
    #    F = F_best
    #else:
    #    F_best = copy.deepcopy(F)    
    #    best_reward = reward1
    backward_process(n, dt, nt, lambdaS, lambdaIU, lambdaIT, S, IU, IT, F)
    #print(F)
    #pdb.set_trace()

#print(optimal)
#pdb.set_trace()
testing_s = pd.DataFrame(data = optimal_cost[0])
testing_s.to_csv('pontryagain_s_sum_test_assortive1_ba20_IC1new.csv', header=False, index = False)
testing_iu = pd.DataFrame(data = optimal_cost[1])
testing_iu.to_csv('pontryagain_iu_sum_test_assortive1_ba20_IC1new.csv', header=False, index = False)
testing_it = pd.DataFrame(data = optimal_cost[2])
testing_it.to_csv('pontryagain_it_sum_test_assortive1_ba20_IC1new.csv', header=False, index = False)
testing_stra = pd.DataFrame(data = optimal)
testing_stra.to_csv('pontryagain_strategy_test_assortive1_ba20_IC1new.csv', header=False, index = False)
#print(optimal[1000])
#print(reward1-reward0)