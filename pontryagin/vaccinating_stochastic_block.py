# -*- coding: utf-8 -*-
"""
Created on Fri May 21 03:56:49 2021

@author: 15000
"""

import copy
#import pdb
import pandas as pd
import csv
import pdb
data1 = csv.reader(open('stochastic_block_pl_full.csv'))
index = []
j = 0
ind_sum = 0
for i in data1:
    if float(i[0]) != 0:
        index.append((float(i[0]), j))
    
    #if j != 0:
    ind_sum += float(i[0])
    j += 1


#for i in range(len(index)):
#    if i != 0:
#        index[i] = (float(index[i][0] / ind_sum), i)
#    else:
#        index[i] = (0, 0)

#index.pop(0)
#index.sort()

correlation = [[0 for i in range(len(index))] for j in range(len(index))]
data1 = csv.reader(open('stochastic_block_plk_full.csv'))
storage = []
for i in data1:
    storage.append(i)

for i in range(len(index)):
    for j in range(len(index)):
        #print(storage[index[i][1]])
        #print(len(storage))
        correlation[i][j] = float(storage[index[j][1]][index[i][1]])

summ = 0
summ1 = 0
var = 0
for i in range(len(index)):
    summ += (index[i][1]) * index[i][0]
    summ1 += index[i][0]
    var += (index[i][1])**2 * index[i][0]
    
var -= summ**2
def ODEsolver(n, S, IU, IT, F, t, dt):
    global S_original
    new_infect = [0 for i in range(n)]
    new_infect1 = [0 for i in range(n)]
    DIU = [0 for i in range(n)]
    DIT = [0 for i in range(n)]
    DIU_new = [0 for i in range(n)]
    DIT_new = [0 for i in range(n)]
    global P, beta_u, beta_t, gamma_u, gamma_t
    new_infect0 = 0
    new_infect_copy = [0 for i in range(n)]
    new_infect1_copy = [0 for i in range(n)]
    m = len(S)
    remains = 0
    index_remain = []
    #for i in range(m):
    #    if F[i]*dt > S[i]:
    #        remains += (F[i]*dt - S[i])/dt
    #        F[i] -= (F[i]*dt - S[i])/dt
    #        index_remain.append(i)
    
    #if remains > 0:
    #    F_supp = Strategy_complement(m, remains, S, IU, IT, lambdaS, lambdaIU, lambdaIT, index_remain, F)
    #    for i in range(len(F)):
    #        if i not in index_remain:
    #            F[i] += F_supp[i]
    #print(sum(F))
    #pdb.set_trace()
    #print(sum(S))
    for i in range(n):
        for j in range(m):
            new_infect[i] += dt* ((index[i][1]) * S[i] * P[i][j] * (beta_u * IU[j]/ S_original[j] + beta_t * IT[j]/ S_original[j])) 
    
    #print(new_infect)
    #pdb.set_trace()
    for i in range(n):
        if new_infect[i] > S[i]:
            new_infect[i] = S[i]
        
        new_infect_copy[i] = new_infect[i]
        new_infect0 += new_infect[i] / 2
        new_infect[i] += F[i] * dt; #* S[i]
        if new_infect[i] > S[i]:
            #print(F, t)
            #pdb.set_trace()
            new_infect[i] = S[i]
    
    #print(sum(new_infect) - sum(new_infect_copy))
    #pdb.set_trace()
    
    
    for i in range(n):
        S[i] -= new_infect[i]
    
    for i in range(n):
        DIU[i] = -(gamma_u * dt ) * IU[i] + new_infect_copy[i]
        DIT[i] = -(gamma_u * dt ) * IT[i] 
        IU[i] = IU[i] + DIU[i]
        IT[i] = IT[i] + DIT[i]
    
    #print(IU)
    #pdb.set_trace()
    for i in range(n):
        for j in range(m):
            new_infect1[i] += dt * ((index[i][1]) * S[i] * P[i][j] * (beta_u * IU[j]/ S_original[j] + beta_t * IT[j]/ S_original[j]))
        
        if new_infect1[i] > S[i]:
            new_infect1[i] = S[i]
            
        new_infect1_copy[i] = new_infect1[i]
        new_infect0 += new_infect1[i] / 2
        new_infect1[i] += F[i] * dt;# * dt * S[i]
        if new_infect1[i] > S[i]:
            new_infect1[i] = S[i]
    
    #print(sum(F))
    #print((sum(new_infect) + sum(new_infect1))/2)
    #pdb.set_trace()
    #print(IU)
    #pdb.set_trace()
    
    for i in range(n):
        S[i] = S[i] - new_infect1[i] / 2 + new_infect[i] / 2
    
    for i in range(n):
        DIU_new[i] = -(gamma_u * dt ) * IU[i] + new_infect1_copy[i]
        DIT_new[i] = -(gamma_u * dt ) * IT[i] 
        IU[i] = IU[i] + DIU_new[i] /2 - DIU[i] / 2
        IT[i] = IT[i] + DIT_new[i] / 2 - DIT[i] / 2
    
    #print(IU)
    #pdb.set_trace()
    #print(sum(S))
    #pdb.set_trace()
    #print(new_infect0, sum(s_cop) - sum(S))
    #pdb.set_trace()
    #print(new_infect0)
    #pdb.set_trace()
    return (S, IU, IT, new_infect0)

def ODEsolver3(n, S, IU, IT, F, t, dt, lambdaS, lambdaIU, lambdaIT):
    global S_original, F_total
    new_infect = [0 for i in range(n)]
    new_infect1 = [0 for i in range(n)]
    DIU = [0 for i in range(n)]
    DIT = [0 for i in range(n)]
    DIU_new = [0 for i in range(n)]
    DIT_new = [0 for i in range(n)]
    global P, beta_u, beta_t, gamma_u, gamma_t
    new_infect0 = 0
    new_infect_copy = [0 for i in range(n)]
    new_infect1_copy = [0 for i in range(n)]
    m = len(S)
    remains = 0
    index_remain = []
    for i in range(m):
        if F[i]*dt > S[i]:
            remains += (F[i]*dt - S[i])/dt
            F[i] -= (F[i]*dt - S[i])/dt
            index_remain.append(i)
    
    strategy = Strategy_complement(m, F_total, S, IU, IT, lambdaS, lambdaIU, lambdaIT, index_remain, F)
    #if remains > 0:
    #    F_supp = Strategy_complement(m, remains, S, IU, IT, lambdaS, lambdaIU, lambdaIT, index_remain, F)
    #    for i in range(len(F)):
    #        if i not in index_remain:
    #            F[i] += F_supp[i]
    F = strategy
    #print(sum(F))
    #pdb.set_trace()
    #print(sum(S))
    for i in range(n):
        for j in range(m):
            new_infect[i] += dt* ((index[i][1]) * S[i] * P[j][i] * (beta_u * IU[j]/ S_original[j] + beta_t * IT[j]/ S_original[j])) 
        
        if new_infect[i] > S[i]:
            new_infect[i] = S[i]
        
        new_infect_copy[i] = new_infect[i]
        new_infect0 += new_infect[i] / 2
        new_infect[i] += F[i] * dt; #* S[i]
        if new_infect[i] > S[i]:
            #print(F, t)
            #pdb.set_trace()
            new_infect[i] = S[i]

    
    for i in range(n):
        S[i] -= new_infect[i]
    
    for i in range(n):
        DIU[i] = -(gamma_u * dt ) * IU[i] + new_infect_copy[i]
        DIT[i] = -(gamma_u * dt ) * IT[i] 
        IU[i] = IU[i] + DIU[i]
        IT[i] = IT[i] + DIT[i]
    
    #print(IU)
    #pdb.set_trace()
    for i in range(n):
        for j in range(m):
            new_infect1[i] += dt * ((index[i][1]) * S[i] * P[j][i] * (beta_u * IU[j]/ S_original[j] + beta_t * IT[j]/ S_original[j]))
        
        if new_infect1[i] > S[i]:
            new_infect1[i] = S[i]
            
        new_infect1_copy[i] = new_infect1[i]
        new_infect0 += new_infect1[i] / 2
        new_infect1[i] += F[i] * dt;# * dt * S[i]
        if new_infect1[i] > S[i]:
            new_infect1[i] = S[i]
    
    #print(sum(F))
    #print((sum(new_infect) + sum(new_infect1))/2)
    #pdb.set_trace()
    #print(IU)
    #pdb.set_trace()
    
    for i in range(n):
        S[i] = S[i] - new_infect1[i] / 2 + new_infect[i] / 2
    
    for i in range(n):
        DIU_new[i] = -(gamma_u * dt ) * IU[i] + new_infect1_copy[i]
        DIT_new[i] = -(gamma_u * dt ) * IT[i] 
        IU[i] = IU[i] + DIU_new[i] /2 - DIU[i] / 2
        IT[i] = IT[i] + DIT_new[i] / 2 - DIT[i] / 2
    
    #print(IU)
    #pdb.set_trace()
    #print(sum(S))
    #pdb.set_trace()
    #print(new_infect0, sum(s_cop) - sum(S))
    #pdb.set_trace()
    return (S, IU, IT, new_infect0, F)

def Strategy_complement(n, F, S, IU, IT, lambdaS, lambdaIU, lambdaIT, index, F_arrange):
    global fmin, fmax, P
    Deter = [0 for i in range(n)]
        
    for i in range(n):
        Deter[i] = -lambdaS[i]
    
    ##if key:
        #print(Deter)
        #pdb.set_trace()
    for i in range(n):
        Deter[i] = (Deter[i], i)
        
    Deter.sort()
    #print(Deter)
    #print(P[0])
    #print(lambdaS, lambdaIU, lambdaIT)
    #pdb.set_trace()
    if F > float(sum(S)) * fmax:
        Budget = [S[i] *fmax  for i in range(n)]
        return Budget
    
    #F = F - fmin 
    #Budget = [0 for i in range(n)]
    #else:
    #    Budget = [S[i]  for i in range(n)]
    #    return Budget
    
    Budget = [fmin * S[i]  for i in range(n)]
    F -= fmin * sum(S)
    #print(F)
    #pdb.set_trace()
    i = 0
    #print(F)
    #print(S)
    #pdb.set_trace()
    while F>0 and i < n:
        if i not in index:
            if F > min((fmax - S[Deter[i][1]] * fmin), max(S[Deter[i][1]], 0)): 
                Budget[Deter[i][1]] += min((fmax - S[Deter[i][1]] * fmin), max(S[Deter[i][1]], 0))
                F -= min((fmax - S[Deter[i][1]] * fmin), max(S[Deter[i][1]], 0)) 
            else:
                Budget[Deter[i][1]] += F
                F -= F
        
        i += 1
    
    #print(Budget)
    #pdb.set_trace()
    return Budget

def ODEsolver2(n, S, IU, IT, F, t, dt):
    global S_original
    new_infect = [0 for i in range(n)]
    new_infect1 = [0 for i in range(n)]
    DIU = [0 for i in range(n)]
    DIT = [0 for i in range(n)]
    DIU_new = [0 for i in range(n)]
    DIT_new = [0 for i in range(n)]
    global P, beta_u, beta_t, gamma_u, gamma_t
    new_infect0 = 0
    new_infect_copy = [0 for i in range(n)]
    new_infect1_copy = [0 for i in range(n)]
    m = len(S)
    #print(sum(F))
    #pdb.set_trace()
    #print(sum(S))
    key = 0
    for i in range(n):
        for j in range(m):
            new_infect[i] += dt* ((index[i][1]) * S[i] * P[j][i] * (beta_u * IU[j]/ S_original[j] + beta_t * IT[j]/ S_original[j])) 
        
        if new_infect[i] > S[i]:
            new_infect[i] = S[i]
        
        new_infect_copy[i] = new_infect[i]
        new_infect0 += new_infect[i] / 2
        new_infect[i] += F[i] * dt; #* S[i]
        if new_infect[i] > S[i]:
            #print(F, t)
            key = 1
            #pdb.set_trace()
            new_infect[i] = S[i]
    
    #print(sum(new_infect) - sum(new_infect_copy))
    #pdb.set_trace()
    
    
    for i in range(n):
        S[i] -= new_infect[i]
    
    if key:
        maxim = max(F)
        for p in range(len(F)):
            if F[p] == maxim:
                #print(p)
                break
                
        #print(S[p], F[p], F, p, t)
        #pdb.set_trace()
        
    for i in range(n):
        DIU[i] = -(gamma_u * dt ) * IU[i] + new_infect_copy[i]
        DIT[i] = -(gamma_u * dt ) * IT[i] 
        IU[i] = IU[i] + DIU[i]
        IT[i] = IT[i] + DIT[i]
    
    #print(IU)
    #pdb.set_trace()
    for i in range(n):
        for j in range(m):
            #print(P[0])
            pdb.set_trace()
            new_infect1[i] += dt * ((index[i][1]) * S[i] *P[j][i] * (beta_u * IU[j]/ S_original[j] + beta_t * IT[j]/ S_original[j]))
        
        if new_infect1[i] > S[i]:
            new_infect1[i] = S[i]
            
        new_infect1_copy[i] = new_infect1[i]
        new_infect0 += new_infect1[i] / 2
        new_infect1[i] += F[i] * dt;# * dt * S[i]
        if new_infect1[i] > S[i]:
            new_infect1[i] = S[i]
    
    for i in range(n):
        S[i] = S[i] - new_infect1[i] / 2 + new_infect[i] / 2
    
    for i in range(n):
        DIU_new[i] = -(gamma_u * dt ) * IU[i] + new_infect1_copy[i]
        DIT_new[i] = -(gamma_u * dt ) * IT[i] 
        IU[i] = IU[i] + DIU_new[i] /2 - DIU[i] / 2
        IT[i] = IT[i] + DIT_new[i] / 2 - DIT[i] / 2
    
    #print(IU)
    #pdb.set_trace()
    #print(sum(S))
    #pdb.set_trace()
    #print(new_infect0, sum(s_cop) - sum(S))
    #pdb.set_trace()
    return (S, IU, IT, new_infect0)

def ODEsolver1(n, S, IU, IT, F, t, dt):
    global S_original
    new_infect = [0 for i in range(n)]
    new_infect1 = [0 for i in range(n)]
    #s_cop = copy.deepcopy(S)
    DIU = [0 for i in range(n)]
    DIT = [0 for i in range(n)]
    DIU_new = [0 for i in range(n)]
    DIT_new = [0 for i in range(n)]
    global P, beta_u, beta_t, gamma_u, gamma_t
    new_infect0 = 0
    new_infect_copy = [0 for i in range(n)]
    new_infect1_copy = [0 for i in range(n)]
    m = len(S)
    #print(sum(S))
    for i in range(n):
        for j in range(m):
            #print()
            #pdb.set_trace()
            new_infect[i] += dt* ((index[i][1]) * S[i] * P[j][i] * (beta_u * IU[j]/ S_original[j] + beta_t * IT[j]/ S_original[j])) 
        
        if new_infect[i] > S[i]:
            new_infect[i] = S[i]
        
        new_infect_copy[i] = new_infect[i]
        new_infect0 += new_infect[i] / 2
        new_infect[i] += F[i] * dt; #* S[i]
        if new_infect[i] > S[i]:
            new_infect[i] = S[i]
    
    #print(sum(new_infect) - sum(new_infect_copy))
    #pdb.set_trace()
    
    
    for i in range(n):
        S[i] -= new_infect_copy[i]
    
    for i in range(n):
        DIU[i] = -(gamma_u * dt ) * IU[i] + new_infect_copy[i]
        DIT[i] = -(gamma_u * dt ) * IT[i] 
        IU[i] = IU[i] + DIU[i]
        IT[i] = IT[i] + DIT[i]
    
    for i in range(n):
        for j in range(m):
            new_infect1[i] += dt * ((index[i][1]) * S[i] * P[j][i] * (beta_u * IU[j]/ S_original[j] + beta_t * IT[j]/ S_original[j]))
        
        if new_infect1[i] > S[i]:
            new_infect1[i] = S[i]
            
        new_infect1_copy[i] = new_infect1[i]
        new_infect0 += new_infect1[i] / 2
        new_infect1[i] += F[i] * dt;# * dt * S[i]
        if new_infect1[i] > S[i]:
            new_infect1[i] = S[i]
    
    #print(IU)
    #pdb.set_trace()
    #print(sum(F))
    #print((sum(new_infect) + sum(new_infect1))/2)
    #pdb.set_trace()
        
    for i in range(n):
        S[i] = S[i] - new_infect1[i] / 2 + new_infect[i] / 2
    
    for i in range(n):
        DIU_new[i] = -(gamma_u * dt ) * IU[i] + new_infect1_copy[i]
        DIT_new[i] = -(gamma_u * dt ) * IT[i] 
        IU[i] = IU[i] + DIU_new[i] /2 - DIU[i] / 2
        IT[i] = IT[i] + DIT_new[i] / 2 - DIT[i] / 2

    #print(IU)
    #pdb.set_trace()
    #print(sum(S))
    #pdb.set_trace()
    #print(new_infect0, sum(s_cop) - sum(S))
    #pdb.set_trace()
    
    return (S, IU, IT, new_infect0)


def Adjointsolver(n, lambdaS, lambdaIU, lambdaIT, S, IU, IT, F, t, dt):
    DLambdaS = [0 for i in range(n)]
    DLambdaS1 = [0 for i in range(n)]
    DLambdaIU = [0 for i in range(n)]
    DLambdaIT = [0 for i in range(n)]
    DLambdaIU_new = [0 for i in range(n)]
    DLambdaIT_new = [0 for i in range(n)]
    global P, beta_u, beta_t, gamma_u, gamma_t, gamma
    m = len(S)
   
    for i in range(n):
        for j in range(m):
            DLambdaS[i] += (gamma**(t-dt) - lambdaS[i] +lambdaIU[i]) * (index[i][1]) * P[i][j] * (beta_u * IU[j] + 
                            beta_t * IT[j]) / S_original[j]  
            DLambdaIU[i] += S[j] * P[j][i] * (index[j][1]) * beta_u * (gamma**t-lambdaS[j] + 
                              lambdaIU[j]) / S_original[i]
            DLambdaIT[i] += S[j] * P[j][i] * (index[j][1]) * beta_t * (gamma**t-lambdaS[j] + lambdaIU[j]) / S_original[i] 
        
        #DLambdaS[i] -= F[i] * lambdaS[i]
        DLambdaIU[i] += - gamma_u * lambdaIU[i] 
        DLambdaIT[i] += - gamma_t * lambdaIT[i] 
    #print(DLambdaIU, S)
    #pdb.set_trace()
    #if t < 2*dt:
    #    print(DLambdaIU, t)
    #    pdb.set_trace()
        
    for i in range(n):
        lambdaS[i] = lambdaS[i] + dt * DLambdaS[i]
        lambdaIU[i] = lambdaIU[i] + dt * DLambdaIU[i]
        lambdaIT[i] = lambdaIT[i] + dt * DLambdaIT[i]
    
    
    for i in range(n):
        for j in range(m):
            DLambdaS1[i] += (gamma**(t-dt) - lambdaS[i] +lambdaIU[i]) * (index[i][1]) * P[i][j] * (beta_u * IU[j] + 
                            beta_t * IT[j]) / S_original[j]  
            DLambdaIU_new[i] += S[j] * P[j][i] * (index[j][1]) * beta_u * (gamma**(t-dt)-lambdaS[j] +
                              lambdaIU[j]) / S_original[i] 
            DLambdaIT_new[i] += S[j] * P[j][i] * (index[j][1]) * beta_t * (gamma**(t-dt) - lambdaS[j] + lambdaIU[j]) / S_original[i] 
        
        #DLambdaS[i] -= F[i] *lambdaS[i]
        DLambdaIU_new[i] += - gamma_u * lambdaIU[i] 
        DLambdaIT_new[i] += - gamma_t * lambdaIT[i]
                                                               
    for i in range(n):  
        lambdaS[i] = lambdaS[i] - dt/2 * DLambdaS[i] + dt/2 * DLambdaS1[i]
        lambdaIU[i] = lambdaIU[i] - dt/2 * DLambdaIU[i] + dt/2 * DLambdaIU_new[i]
        lambdaIT[i] = lambdaIT[i] - dt/2 * DLambdaIT[i] + dt/2 * DLambdaIT_new[i]
    
    return (lambdaS, lambdaIU, lambdaIT)

def Strategy(n, F, S, IU, IT, lambdaS, lambdaIU, lambdaIT, key=0):
    global fmin, fmax, P
    Deter = [0 for i in range(n)]
    #if key == 1:
        #print(lambdaIU, lambdaIT, lambdaS)   
    #    pdb.set_trace()
        
    for i in range(n):
        Deter[i] = -lambdaS[i]# * S[i]#-lambdaIU[i] * IU[i]/S_original[i] + lambdaIT[i] * IU[i]/S_original[i]
        #Deter[i] = -Deter[i]
    
    ##if key:
        #print(Deter)
        #pdb.set_trace()
    for i in range(n):
        Deter[i] = (Deter[i], i)
        
    Deter.sort()
    #print(lambdaS, lambdaIU, lambdaIT)
    #pdb.set_trace()
    if F > float(sum(S)):
        Budget = [S[i]  for i in range(n)]
        return Budget
    
    F0 = F
    #print(sum(S))
    F0 = F0 - fmin *sum(S)
    
    #pdb.set_trace()
    Budget = [fmin * S[i]  for i in range(n)]
    #else:
    #    Budget = [S[i]  for i in range(n)]
    #    return Budget
    
    #Budget = [fmin * S[i]  for i in range(n)]
    i = 0
    #print(F)
    #print(S)
    #pdb.set_trace()
    while F0>0 and i < n:
        if F0 > (fmax - fmin)* max(S[Deter[i][1]], 0): 
            Budget[Deter[i][1]] += (fmax - fmin)* max(S[Deter[i][1]], 0)
            F0 -= (fmax - fmin)*max(S[Deter[i][1]], 0) 
        else:
            Budget[Deter[i][1]] += F0
            F0 -= F0
        
        #if F > S[Deter[i][1]] * (fmax - fmin): 
        #    Budget[Deter[i][1]] += (fmax - fmin) * max(S[Deter[i][1]], 0) 
        #    F -= (fmax-fmin) * max(S[Deter[i][1]], 0)
        #else:
        #    Budget[Deter[i][1]] += F
        #    F -= (fmax - fmin) * max(S[Deter[i][1]], 0)
        
    #    print(F, Budget)
        i += 1
        
    #print(1)
    #print(sum(Budget))
    #print(Budget)
    #print(Budget)
    #print([Budget[i] / S[i] for i in range(n)])
    #pdb.set_trace()
    return Budget

def evaluation(n, S0, S0_copy, results, F, t):
    global gamma, dt
    #y = 0
    #for i in range(n):
    #    y += (S[i] - S_new[i])# - F[i] * dt
        
    #print(sum(S0) - sum(S0_copy))
    #print(results)
    #pdb.set_trace()
    y = results * gamma**t
    return y

def evaluation1(n, S0, S0_copy, results, F, t):
    global gamma, dt
    y = results 
    return y


def forward_process2(n, dt, nt, S0, IU0, IT0, F, key=0):
    t = 0
    reward = 0
    reward1 = 0
    global S, IU, IT, LS, LIU, LIT
    if not key:
        for i in range(nt):
            S0_copy = copy.deepcopy(S0)
            F[i] = Strategy(n, F_total, S[i], IU[i], IT[i], LS[nt-i], LIU[nt-i], LIT[nt-i])
            #print(S[i])
            #print(F_total)
            #pdb.set_trace()
            Result = ODEsolver(n, S0, IU0, IT0, F[i], t, dt)
            #print(sum(S0))
            #print(sum(F[i]))
            #pdb.set_trace()
            Re.append(reward1)
            reward += evaluation(n, S0, S0_copy, Result[3], F[i], t)
            reward1 += evaluation1(n, S0, S0_copy, Result[3], F[i], t)
            #print(S0_copy)
            #print(Result[0])
            #print(evaluation(n, S0_copy, Result[0], t))
            #print(sum(Result[0]) - sum(S0_copy))
            #pdb.set_trace()
            #print(reward1 + sum(S0))
            S0 = Result[0]
            IU0 = Result[1]
            IT0 = Result[2]
            #F[i] = Result[4]
            t += dt
            S.append(copy.deepcopy(S0))
            IU.append(copy.deepcopy(IU0))
            IT.append(copy.deepcopy(IT0))
    else:
        for i in range(nt):
            S0_copy = copy.deepcopy(S0)
            #print(sum(S0))
            F[i] = Strategy(n, F_total, S[i], IU[i], IT[i], LS[nt-i], LIU[nt-i], LIT[nt-i])
            Result = ODEsolver(n, S0, IU0, IT0, F[i], t, dt)
            #print(sum([min(fmax, F_total/sum(S0)) * S0[i] for i in range(n)]))
            #pdb.set_trace()
            Re.append(reward1)
            reward += evaluation(n, S0, S0_copy, Result[3], [min(fmax, F_total/sum(S0)) * S0[i] for i in range(n)], t)
            reward1 += evaluation1(n, S0, S0_copy, Result[3], F[i], t)
            #print(Result[3])
            #pdb.set_trace()
            #print(S0_copy)
            #print(Result[3])
            #print(evaluation(n, S0_copy, Result[0], t))
            #print(sum(Result[0]) - sum(S0_copy))
            #pdb.set_trace()
            #print(reward1 + sum(S0))
            S0 = Result[0]
            IU0 = Result[1]
            IT0 = Result[2]
            #F[i] = Result[4]
            t += dt
            
            S.append(copy.deepcopy(S0))
            IU.append(copy.deepcopy(IU0))
            IT.append(copy.deepcopy(IT0))
        #print(IT0)
        #pdb.set_trace()
        #print(sum(S0))
    
    #print(S)
    #pdb.set_trace()

    return (S0, IU0, IT0, reward)

def forward_process(n, dt, nt, S0, IU0, IT0, F, key=0):
    t = 0
    reward = 0
    reward1 = 0
    global S, IU, IT
    if not key:
        for i in range(nt):
            S0_copy = copy.deepcopy(S0)
            #print(sum(S0))
            Result = ODEsolver(n, S0, IU0, IT0, F[i], t, dt)
            #print(sum(S0))
            # pdb.set_trace()
            Re.append(reward1)
            reward += evaluation(n, S0, S0_copy, Result[3], F[i], t)
            reward1 += evaluation1(n, S0, S0_copy, Result[3], F[i], t)
            #print(S0_copy)
            #print(Result[0])
            #print(evaluation(n, S0_copy, Result[0], t))
            #print(sum(Result[0]) - sum(S0_copy))
            #pdb.set_trace()
            #print(reward1 + sum(S0))
            S0 = Result[0]
            IU0 = Result[1]
            IT0 = Result[2]
            t += dt
            S.append(copy.deepcopy(S0))
            IU.append(copy.deepcopy(IU0))
            IT.append(copy.deepcopy(IT0))
    else:
        for i in range(nt):
            S0_copy = copy.deepcopy(S0)
            #print(sum(S0))
            Result = ODEsolver(n, S0, IU0, IT0, [min(fmax, F_total/sum(S0)) * S0[i] for i in range(n)], t, dt)
            #print(sum([min(fmax, F_total/sum(S0)) * S0[i] for i in range(n)]))
            #pdb.set_trace()
            Re.append(reward1)
            reward += evaluation(n, S0, S0_copy, Result[3], [min(fmax, F_total/sum(S0)) * S0[i] for i in range(n)], t)
            reward1 += evaluation1(n, S0, S0_copy, Result[3], F[i], t)
            #print(Result[3])
            #pdb.set_trace()
            #print(S0_copy)
            #print(Result[3])
            #print(evaluation(n, S0_copy, Result[0], t))
            #print(sum(Result[0]) - sum(S0_copy))
            #pdb.set_trace()
            #print(reward1 + sum(S0))
            S0 = Result[0]
            IU0 = Result[1]
            IT0 = Result[2]
            t += dt
            
            S.append(copy.deepcopy(S0))
            IU.append(copy.deepcopy(IU0))
            IT.append(copy.deepcopy(IT0))
        #print(IT0)
        #pdb.set_trace()
        #print(sum(S0))
    
    #print(S)
    #pdb.set_trace()

    return (S0, IU0, IT0, reward)

def forward_process_de(n, dt, nt, S0, IU0, IT0, F, key=0):
    t = 0
    reward = 0
    reward1 = 0
    global S, IU, IT, F_total
    if not key:
        for i in range(nt):
            S0_copy = copy.deepcopy(S0)
            #print(sum(S0))
#            if i < 200:
#                F1 = [0 for i in range(n)]
#            else:
            if sum(S0) > F_total:
                F1 = [F_total/sum(S0) * S0[i] for i in range(n)]
            else:
                F1 = [S0[i] for i in range(n)]
            
            F[i] = F1 
            Result = ODEsolver(n, S0, IU0, IT0, F1, t, dt)
            #print(sum(S0))
            # pdb.set_trace()
            Re.append(reward1)
            reward += evaluation(n, S0, S0_copy, Result[3], F[i], t)
            reward1 += evaluation1(n, S0, S0_copy, Result[3], F[i], t)
            #print(reward1)
            pdb.set_trace()
            #print(S0_copy)
            #print(Result[0])
            #print(evaluation(n, S0_copy, Result[0], t))
            #print(sum(Result[0]) - sum(S0_copy))
            #pdb.set_trace()
            S0 = Result[0]
            IU0 = Result[1]
            IT0 = Result[2]
            t += dt
            S.append(copy.deepcopy(S0))
            IU.append(copy.deepcopy(IU0))
            IT.append(copy.deepcopy(IT0))
    else:
        for i in range(nt):
            S0_copy = copy.deepcopy(S0)
            #print(sum(S0))
            #if i < 200:
            #    F1 = [0 for i in range(n)]
            #else:
            if sum(S0) > F_total:
                F1 = [F_total/sum(S0) * S0[i] for i in range(n)]
            else:
                F1 = [S0[i] for i in range(n)]
            
            F[i] = F1 
            Result = ODEsolver(n, S0, IU0, IT0, F1, t, dt)
            #print(sum([min(fmax, F_total/sum(S0)) * S0[i] for i in range(n)]))
            #F1 = [float(F_total * S0[i]/sum(S0)) for i in range(len(S0))]
            #pdb.set_trace()
            Re.append(reward1)
            reward += evaluation(n, S0, S0_copy, Result[3], [min(fmax, F_total/sum(S0)) * S0[i] for i in range(n)], t)
            reward1 += evaluation1(n, S0, S0_copy, Result[3], F[i], t)
            #print(Result[3])
            #pdb.set_trace()
            
            #print(S0_copy)
            #print(Result[3])
            #print(evaluation(n, S0_copy, Result[0], t))
            #print(sum(Result[0]) - sum(S0_copy))
            #pdb.set_trace()
            S0 = Result[0]
            IU0 = Result[1]
            IT0 = Result[2]
            #print(reward1 + sum(S0))
            t += dt
            
            S.append(copy.deepcopy(S0))
            IU.append(copy.deepcopy(IU0))
            IT.append(copy.deepcopy(IT0))
        #print(IT0)
        #pdb.set_trace()
        #print(sum(S0))
    
    #print(S)
    #pdb.set_trace()

    return (S0, IU0, IT0, reward)


def backward_process(n, dt, nt, lambdaS, lambdaIU, lambdaIT, S, IU, IT, F):
    t = T
    global F_total
    LS = []
    LIU = []
    LIT = []
    LS.append(copy.deepcopy(lambdaS))
    LIU.append(copy.deepcopy(lambdaIU))
    LIT.append(copy.deepcopy(lambdaIT))
    for i in range(nt):
        #F_copy = Strategy(n, F_total, S[nt-i], IU[nt-i], IT[nt-i], lambdaS, lambdaIU, lambdaIT)
        Result = Adjointsolver(n, lambdaS, lambdaIU, lambdaIT, S[nt-i], IU[nt-i], IT[nt-i], F[nt-1-i], t, dt)
        lambdaS = Result[0]
        lambdaIU = Result[1]
        lambdaIT = Result[2]
        LS.append(copy.deepcopy(lambdaS))
        LIU.append(copy.deepcopy(lambdaIU))
        LIT.append(copy.deepcopy(lambdaIT))
        t -= dt
        
        #print(lambdaS, lambdaIU, lambdaIT)
        #pdb.set_trace()
        #F[nt-i] = F_copy
    
    #print(LIU)
    #pdb.set_trace()
    for i in range(nt+1):
        #if i != 1:
        F[nt-i] = Strategy(n, F_total, S[nt-i], IU[nt-i], IT[nt-i], LS[i], LIU[i], LIT[i])
        #print(F[nt-i])
        #pdb.set_trace()
        #else:
        #    F[nt-i] = Strategy(n, F_total, S[nt-i], IU[nt-i], IT[nt-i], LS[i], LIU[i], LIT[i], 1)
        #####
    #print(LIU[2], LIT[2], IU[0])
    #print(lambdaIU)
    #pdb.set_trace()
    #F[0] = Strategy(n, F_total, S[0], IU[0], IT[0], lambdaS, lambdaIU, lambdaIT)
    #F_sum = [sum(F[i]) for i in range(nt+1)]
    
    #print(F[0:100])
    #pdb.set_trace()
    return (LS, LIU, LIT)


n = len(index)
S_original = [index[i][0] / summ1 for i in range(n)]#[(1-1e-6)/8 for i in range(8)]
#summSori = sum(S_original)
#S0 = [(1-1e-6)*S_original[i] / summSori for i in range(n)]
S0 = [(1-1e-6)*S_original[i] for i in  range(n)]
#print(S_original)
#pdb.set_trace()
#S0 = [(1-1e-6)*S_original[i] / summSori for i in  range(8)]
IU0 = [S_original[i] * 1e-6 for i in range(n)]
S0 = [S_original[i] for i in range(n)]
S0[1] = S0[1]  - 1e-6
IU0 = [0 for i in range(n)]
IU0[1] = 1e-6
IT0 = [0 for i in range(n)]
gamma = 0.95
lambdaS = [0 for i in range(n)]
lambdaIU = [0 for i in range(n)]
lambdaIT = [0 for i in range(n)]


fmin = 0.000
fmax = 0.4
F_total = 0.003
gamma_u = 1/14
gamma_t = 1/14
R0 = 4.5
beta_u = gamma_u / ((summ - 1 + var/summ) / R0 - 1)
#beta_u = 3*gamma_u / summ
beta_u = 0.0130
beta_t = beta_u / 10
rho = [0 for i in range(n)]
for i in range(n):
    rho[i] = S0[i] * (i+1)

sum_rho = sum(rho)

#P =[[0 for j in range(n)] for i in range(n)]
#for i in range(n):
#    for j in range(n):
#        P[i][j] = (j+1) * S0[j] / sum_rho
        
P = correlation

#print(P)
#pdb.set_trace()
T = 1511
dt = 0.1
nt = 1511
#F_total = 0
F = [[F_total * S_original[i]/summ1  for i in range(8)] for j in range(nt+1)]
S = []
IU = []
IT = []
Re = []

S.append(copy.deepcopy(S0))
IU.append(copy.deepcopy(IU0))
IT.append(copy.deepcopy(IT0))
#print(S0)
Iter1 = forward_process_de(n, dt, nt, S0, IU0, IT0, F, 1)
#print(S[0])
testing_re = pd.DataFrame(data = Re)
testing_re.to_csv('pontryagain_re_vaccine_sb20_IC1new.csv')
testing_s = pd.DataFrame(data = S)
testing_s.to_csv('pontryagain_s_vaccine_sb20_IC1new.csv', header=False, index = False)
testing_s.to_csv('pontryagain_s_vaccine_sb20_IC1new.csv', header=False, index = False)
testing_iu = pd.DataFrame(data = IU)
testing_iu.to_csv('pontryagain_iu_vaccine_sb20_IC1new.csv', header=False, index = False)
testing_it = pd.DataFrame(data = IT)
testing_it.to_csv('pontryagain_it_vaccine_sb20_IC1new.csv', header=False, index = False)
testing_stra = pd.DataFrame(data = F)
testing_stra.to_csv('pontryagain_strategy_vaccine_sb20_IC1new.csv', header=False, index = False)
#pdb.set_trace()
reward0 = Iter1[-1]
#optimal = F
#optimal_cost = [S, IU, IT]
print(reward0)
#print(S[-1])
#pdb.set_trace()
best_reward = reward0
#pdb.set_trace()
LS, LIU, LIT = backward_process(n, dt, nt, lambdaS, lambdaIU, lambdaIT, S, IU, IT, F)

#print(IU)
#print(lambdaIU, lambdaIT)
#print(F[0])
#pdb.set_trace()
#F_best = copy.deepcopy(F)
S_original = [index[i][0] / summ1 for i in range(n)]#[(1-1e-6)/8 for i in range(8)]
#summSori = sum(S_original)
#S0 = [(1-1e-6)*S_original[i] / summSori for i in range(n)]
S0 = [(1-1e-6)*S_original[i] for i in range(n)]
S0 = [S_original[i] for i in range(n)]
S0[1] = S0[1]  - 1e-6
IU0 = [0 for i in range(n)]
IU0[1] = 1e-6
IU0 = [S0[i] * 1e-6 for i in range(n)]
IT0 = [0 for i in range(n)]
lambdaS = [0 for i in range(n)]
lambdaIU = [0 for i in range(n)]
lambdaIT = [0 for i in range(n)]
S = []
IU = []
IT = []
Re = []
S.append(copy.deepcopy(S0))
IU.append(copy.deepcopy(IU0))
IT.append(copy.deepcopy(IT0))
Iter2 = forward_process2(n, dt, nt, S0, IU0, IT0, F)
reward1 = Iter2[-1]
#print(F)
print(reward1)
#pdb.set_trace()

#if reward1 < best_reward:
Fcopyy = []
#print(F[0], S[0], sum(F[0]))
#pdb.set_trace()
for j in range(nt):
    Fcop = []
    for i in range(n):
        if S[j][i] > 0:
            Fcop.append(F[j][i] / S[j][i])
        else:
            Fcop.append(0)
    
    #print(F[j], S[j], Fcop)
    #pdb.set_trace()
    Fcopyy.append(Fcop)
                
optimal = copy.deepcopy(Fcopyy)
LS, LIU, LIT = backward_process(n, dt, nt, lambdaS, lambdaIU, lambdaIT, S, IU, IT, F)
optimal_cost = [copy.deepcopy(S), copy.deepcopy(IU), copy.deepcopy(IT), copy.deepcopy(Re)]
#else:
#    F_best = copy.deepcopy(F)    
#    best_reward = reward1

#F_copy = copy.deepcopy(F)
#print(reward0)
#print(reward1)
#print(F)
#pdb.set_trace()
#print(F)
#pdb.set_trace()
k = 0
while abs(reward0 - reward1) > 1e-9 and k < 10:
#while k < 10:
    k += 1
    print(reward1)
    #if reward1 < 0.65:
    #    print(F)
    #print(F[-2])
    #pdb.set_trace()
    #print(F)  
    #pdb.set_trace()
    reward0 = reward1
    S_original = [index[i][0] / summ1 for i in range(n)]#[(1-1e-6)/8 for i in range(8)]
    #summSori = sum(S_original)
    #S0 = [(1-1e-6)*S_original[i] / summSori for i in range(n)]
    S0 = [(1-1e-6)*S_original[i] for i in  range(n)]
    #print(S0)
    #pdb.set_trace()
    IU0 = [S0[i] * 1e-6 for i in range(n)]
    S0 = [S_original[i] for i in range(n)]
    S0[1] = S0[1]  - 1e-6
    IU0 = [0 for i in range(n)]
    IU0[1] = 1e-6
    IT0 = [0 for i in range(n)]
    lambdaS = [0 for i in range(n)]
    lambdaIU = [0 for i in range(n)]
    lambdaIT = [0 for i in range(n)]
    S = []
    IU = []
    IT = []
    Re = []
    S.append(copy.deepcopy(S0))
    IU.append(copy.deepcopy(IU0))
    IT.append(copy.deepcopy(IT0))
    Iter2 = forward_process2(n, dt, nt, S0, IU0, IT0, F)
    reward1 = Iter2[-1]
    
    
    if reward1 < best_reward:
        Fcopyy = []
        for j in range(nt):
            Fcop = []
            for i in range(n):
                if S[j][i] > 0:
                    Fcop.append(F[j][i] / S[j][i])
                else:
                    Fcop.append(0)
            Fcopyy.append(Fcop)
                
        optimal = copy.deepcopy(Fcopyy)
        optimal_cost = [copy.deepcopy(S), copy.deepcopy(IU), copy.deepcopy(IT), copy.deepcopy(Re)]
    
    LS, LIU, LIT = backward_process(n, dt, nt, lambdaS, lambdaIU, lambdaIT, S, IU, IT, F)
    #if reward1 > best_reward:
    #    F = F_best
    #else:
    #    F_best = copy.deepcopy(F)    
    #    best_reward = reward1
   #print(optimal[0])
    #pdb.set_trace()

#print(len(optimal_cost))
#pdb.set_trace()
testing_s = pd.DataFrame(data = optimal_cost[3])
testing_s.to_csv('pontryagain_re_sum_test_vaccine_sb20_IC1new.csv', header=False, index = False)
testing_s = pd.DataFrame(data = optimal_cost[0])
testing_s.to_csv('pontryagain_s_sum_test_vaccine_sb20_IC1new.csv', header=False, index = False)
testing_iu = pd.DataFrame(data = optimal_cost[1])
testing_iu.to_csv('pontryagain_iu_sum_test_vaccine_sb20_IC1new.csv', header=False, index = False)
testing_it = pd.DataFrame(data = optimal_cost[2])
testing_it.to_csv('pontryagain_it_sum_test_vaccine_sb20_IC1new.csv', header=False, index = False)
testing_stra = pd.DataFrame(data = optimal)
testing_stra.to_csv('pontryagain_strategy_test_vaccine_sb20_IC1new.csv', header=False, index = False)
#print(reward1)
print(sum(F[0]))
#s = torch.tensor([(1-10**(-5))*1/8 for i in range(8)])
#iu = torch.tensor([(10**(-5))*1/8 for i in range(8)])
#it = torch.tensor([0. for i in range(8)])