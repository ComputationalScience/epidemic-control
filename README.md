# Controlling epidemics through optimal allocation of test kits and vaccine doses across networks

This project uses an effective-degree formulation of epidemic processes on networks to study testing and vaccination interventions.

The file `conditional_degree_distribution.py` generates conditional and joint degree distributions for different networks. 

The basic reproduction number for the studied effective degree models can be determined with the next-generation matrix method as implemented in `next_generation/...`.

Reinforcement-learning-based interventions are implemented in the `RL/RL_testing...` and `RL/RL_vaccination...` files.

Interventions that are based on Pontryagin's maximum principle are available in `pontryagin/testing...` and `pontryagin/vaccinating...`.


Please cite our work if you find our implementations helpful for your own research.

```
@article{xia2022controlling,
  title={Controlling epidemics through optimal allocation of test kits and vaccine doses across networks},
  author={Xia, Mingtao and B{\"o}ttcher, Lucas and Chou, Tom},
  journal={IEEE Transactions on Network Science and Engineering},
  volume={9},
  number={3},
  pages={1422--1436},
  year={2022},
  publisher={IEEE}
}
```
