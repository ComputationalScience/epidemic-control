function beta_calculate
matrix = csvread('barabasi_albert_plk_full.csv');
vector = csvread('barabasi_albert_pl_full.csv');
[m, ~] = size(matrix);
%disp(matrix)
matrix1 = zeros(m);
%for i=1:m
%    vector(i) = vector(i) * (i-1);
%end
summ  = sum(vector);
for i=1:m
    for j=1:m
        if vector(i) ~= 0
            matrix(i, j) = matrix(i, j) * (j-1) * vector(j) / vector(i)  ;
            %matrix(i, j) = (i-1) /summ * (j-1) * vector(j);
        end
    end
end

%disp(matrix)
R0 = 4.5;
gamma = 1/14;
[~, eigen] = eig(matrix);
eigen = max(max(eigen));
%eig = norm(matrix, 2);

%pause
%pause
beta = R0*gamma/eigen;
disp(beta)
%disp(matrix)
end